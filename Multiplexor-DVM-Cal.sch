EESchema Schematic File Version 4
LIBS:Multiplexor-DVM-Cal-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Multiplexor para calibración multímetro - Calibrador"
Date "2022-02-19"
Rev "0"
Comp "INTI"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3100 1800 1750 1100
U 621BA2C5
F0 "MultiVariable" 50
F1 "MultiVariable.sch" 50
F2 "InputHigh" B R 4850 1950 50 
F3 "InputLow" B R 4850 2100 50 
F4 "SenseHigh" I R 4850 2250 50 
F5 "SenseLow" B R 4850 2400 50 
F6 "ResetNormal" I L 3100 1900 50 
F7 "NormalHigh" I L 3100 2450 50 
F8 "SetNormal" I L 3100 2100 50 
F9 "NormalLow" I L 3100 2550 50 
F10 "ResetAux" I L 3100 2000 50 
F11 "AuxHigh" I L 3100 2650 50 
F12 "SetAux" I L 3100 2200 50 
F13 "AuxLow" I L 3100 2750 50 
$EndSheet
$Sheet
S 4500 3800 1700 2800
U 621CE88D
F0 "Corriente" 50
F1 "Corriente.sch" 50
F2 "CurrentLowDirect" I R 6200 3900 50 
F3 "CurrentHighDirect1" I R 6200 4050 50 
F4 "CurrentHighDirect2" I R 6200 4150 50 
F5 "Current4" I R 6200 4600 50 
F6 "Current3" I R 6200 4500 50 
F7 "Current2" I R 6200 4400 50 
F8 "Current1" I R 6200 4300 50 
F9 "CurrentLowSignalSet" I R 6200 4750 50 
F10 "CurrentLowSignalReset" I R 6200 4850 50 
F11 "ResetCurrent4" I R 6200 5100 50 
F12 "SetCurrent4" I R 6200 5000 50 
F13 "ResetCurrent3" I R 6200 5350 50 
F14 "SetCurrent3" I R 6200 5250 50 
F15 "SetCurrent2" I R 6200 5500 50 
F16 "ResetCurrent2" I R 6200 5600 50 
F17 "ResetCurrent1" I R 6200 5850 50 
F18 "SetCurrent1" I R 6200 5750 50 
F19 "AuxHigh" I L 4500 4000 50 
F20 "20A" I L 4500 4300 50 
F21 "SetFrontZero" I R 6200 6100 50 
F22 "ResetFrontZero" I R 6200 6000 50 
F23 "FrontCurrent" I L 4500 5500 50 
F24 "FrontInputLow" I L 4500 5350 50 
F25 "AuxLow" I L 4500 4150 50 
F26 "FrontInputHigh" I L 4500 5200 50 
F27 "CurrentIN" I R 6200 6450 50 
F28 "Guard" I L 4500 6350 50 
F29 "SetGuard" I R 6200 6250 50 
F30 "ResetGuard" I R 6200 6350 50 
$EndSheet
$Sheet
S 5500 1800 1950 1300
U 6246F77A
F0 "Multivariable2" 62
F1 "Multivariable2.sch" 74
F2 "InputHigh" B L 5500 1950 50 
F3 "InputLow" B L 5500 2100 50 
F4 "SenseHigh" B L 5500 2250 50 
F5 "SenseLow" B L 5500 2400 50 
F6 "SetGuard" I R 7450 1900 50 
F7 "ResetGuard" I R 7450 2000 50 
F8 "SetZeroInput" I R 7450 2150 50 
F9 "ResetZeroInput" I R 7450 2250 50 
F10 "SetZeroSense" I R 7450 2400 50 
F11 "ResetZeroSense" I R 7450 2500 50 
F12 "SetZeroLow" I R 7450 2650 50 
F13 "ResetZeroLow" I R 7450 2750 50 
F14 "Guard" I L 5500 2550 50 
F15 "SetZeroHigh" I R 7450 2900 50 
F16 "ResetZeroHigh" I R 7450 3000 50 
$EndSheet
Wire Wire Line
	4850 1950 4950 1950
Wire Wire Line
	4850 2100 5050 2100
Wire Wire Line
	4850 2250 5150 2250
$Comp
L Connector_Generic:Conn_01x02 NormalHigh1
U 1 1 6248F5F4
P 1350 1700
F 0 "NormalHigh1" H 1700 1600 50  0000 C CNN
F 1 "Conn_01x02" H 1700 1700 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 1700 50  0001 C CNN
F 3 "~" H 1350 1700 50  0001 C CNN
	1    1350 1700
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 NormaLow1
U 1 1 6248F6BF
P 1350 2050
F 0 "NormaLow1" H 1700 1950 50  0000 C CNN
F 1 "Conn_01x02" H 1700 2050 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 2050 50  0001 C CNN
F 3 "~" H 1350 2050 50  0001 C CNN
	1    1350 2050
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 AuxHigh1
U 1 1 6248F6E1
P 1350 2350
F 0 "AuxHigh1" H 1650 2250 50  0000 C CNN
F 1 "Conn_01x02" H 1700 2350 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 2350 50  0001 C CNN
F 3 "~" H 1350 2350 50  0001 C CNN
	1    1350 2350
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 AuxLow1
U 1 1 6248F709
P 1350 2650
F 0 "AuxLow1" H 1650 2650 50  0000 C CNN
F 1 "Conn_01x02" H 1650 2750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 2650 50  0001 C CNN
F 3 "~" H 1350 2650 50  0001 C CNN
	1    1350 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1550 2750 1550 2650
Wire Wire Line
	1550 2550 1550 2650
Connection ~ 1550 2650
Wire Wire Line
	1950 2650 1950 2350
Wire Wire Line
	1950 2350 1550 2350
Wire Wire Line
	1550 2250 1550 2350
Wire Wire Line
	3100 2550 2150 2550
Wire Wire Line
	2150 2550 2150 2050
Wire Wire Line
	2150 2050 1550 2050
Wire Wire Line
	1550 1950 1550 2050
Wire Wire Line
	3100 2450 2250 2450
Wire Wire Line
	2250 2450 2250 1700
Wire Wire Line
	2250 1700 1550 1700
Wire Wire Line
	1550 1600 1550 1700
Connection ~ 1550 1700
$Comp
L Connector_Generic:Conn_01x02 20A1
U 1 1 6248FE0F
P 1350 4750
F 0 "20A1" H 1350 4450 50  0000 C CNN
F 1 "Conn_01x02" H 1400 4550 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 4750 50  0001 C CNN
F 3 "~" H 1350 4750 50  0001 C CNN
	1    1350 4750
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 FrontInputHigh1
U 1 1 624915A5
P 1350 5200
F 0 "FrontInputHigh1" H 1270 4875 50  0000 C CNN
F 1 "Conn_01x02" H 1270 4966 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 5200 50  0001 C CNN
F 3 "~" H 1350 5200 50  0001 C CNN
	1    1350 5200
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 FrontInputLow1
U 1 1 624915E7
P 1350 5650
F 0 "FrontInputLow1" H 1450 5350 50  0000 C CNN
F 1 "Conn_01x02" H 1400 5450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 5650 50  0001 C CNN
F 3 "~" H 1350 5650 50  0001 C CNN
	1    1350 5650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 FrontCurrent1
U 1 1 62491619
P 1350 6100
F 0 "FrontCurrent1" H 1270 5775 50  0000 C CNN
F 1 "Conn_01x02" H 1270 5866 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 6100 50  0001 C CNN
F 3 "~" H 1350 6100 50  0001 C CNN
	1    1350 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	1550 5200 1550 5100
Wire Wire Line
	1550 5650 1550 5600
Wire Wire Line
	1550 6100 1550 6050
$Comp
L Connector_Generic:Conn_01x02 InputHigh1
U 1 1 62492FBC
P 4600 1450
F 0 "InputHigh1" H 4520 1125 50  0000 C CNN
F 1 "Conn_01x02" H 4520 1216 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4600 1450 50  0001 C CNN
F 3 "~" H 4600 1450 50  0001 C CNN
	1    4600 1450
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 InputLow1
U 1 1 624930FC
P 4550 850
F 0 "InputLow1" H 4470 525 50  0000 C CNN
F 1 "Conn_01x02" H 4470 616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4550 850 50  0001 C CNN
F 3 "~" H 4550 850 50  0001 C CNN
	1    4550 850 
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 SenseHigh1
U 1 1 62493138
P 5650 700
F 0 "SenseHigh1" H 5730 692 50  0000 L CNN
F 1 "Conn_01x02" H 5730 601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5650 700 50  0001 C CNN
F 3 "~" H 5650 700 50  0001 C CNN
	1    5650 700 
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 SenseLow1
U 1 1 624931A0
P 5650 1000
F 0 "SenseLow1" H 5730 992 50  0000 L CNN
F 1 "Conn_01x02" H 5730 901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5650 1000 50  0001 C CNN
F 3 "~" H 5650 1000 50  0001 C CNN
	1    5650 1000
	1    0    0    -1  
$EndComp
Connection ~ 4950 1950
Wire Wire Line
	4950 1950 5500 1950
Wire Wire Line
	4800 1450 4950 1450
Wire Wire Line
	4800 1350 4800 1450
Connection ~ 4800 1450
Wire Wire Line
	4750 850  5050 850 
Connection ~ 5050 2100
Wire Wire Line
	5050 2100 5500 2100
Wire Wire Line
	4750 750  4750 850 
Wire Wire Line
	5450 800  5150 800 
Connection ~ 5150 2250
Wire Wire Line
	5150 2250 5500 2250
Wire Wire Line
	5450 700  5450 800 
Connection ~ 5450 800 
Wire Wire Line
	5450 1100 5250 1100
Wire Wire Line
	5450 1000 5450 1100
Connection ~ 5450 1100
Connection ~ 4750 850 
Wire Wire Line
	4850 2400 5250 2400
Connection ~ 5250 2400
Wire Wire Line
	5250 2400 5500 2400
$Comp
L Connector_Generic:Conn_01x02 Guard1
U 1 1 624A450B
P 5650 1300
F 0 "Guard1" H 5730 1292 50  0000 L CNN
F 1 "Conn_01x02" H 5730 1201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5650 1300 50  0001 C CNN
F 3 "~" H 5650 1300 50  0001 C CNN
	1    5650 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 1300 5450 1400
Wire Wire Line
	5450 1400 5350 1400
Wire Wire Line
	5350 2550 5500 2550
Wire Wire Line
	1950 2650 3100 2650
Wire Wire Line
	1550 2750 3100 2750
$Comp
L Connector_Generic:Conn_01x02 AuxLowCurrent1
U 1 1 624A69BF
P 1350 4200
F 0 "AuxLowCurrent1" H 1270 3875 50  0000 C CNN
F 1 "Conn_01x02" H 1270 3966 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 4200 50  0001 C CNN
F 3 "~" H 1350 4200 50  0001 C CNN
	1    1350 4200
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 AuxHighCurrent1
U 1 1 624A6A3D
P 1350 3700
F 0 "AuxHighCurrent1" H 1270 3375 50  0000 C CNN
F 1 "Conn_01x02" H 1270 3466 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 3700 50  0001 C CNN
F 3 "~" H 1350 3700 50  0001 C CNN
	1    1350 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	1550 4750 1550 4700
Wire Wire Line
	1550 4200 1550 4150
Wire Wire Line
	1550 3700 1550 3650
$Comp
L conn_2rows-44pins:Conn_2Rows-42Pins J1
U 1 1 62506DF3
P 9550 2100
F 0 "J1" H 9600 3417 50  0000 C CNN
F 1 "Conn_2Rows-42Pins" H 9600 3326 50  0000 C CNN
F 2 "Multiplexor-DVM-Cal:Con_Card_Mux_2" H 9550 2100 50  0001 C CNN
F 3 "~" H 9550 2100 50  0001 C CNN
	1    9550 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1900 2450 1900
Wire Wire Line
	3100 2000 2450 2000
Wire Wire Line
	3100 2100 2450 2100
Wire Wire Line
	3100 2200 2450 2200
Text Label 2550 1900 0    50   ~ 0
ResetNormal
Text Label 2550 2000 0    50   ~ 0
ResetAux
Text Label 2550 2100 0    50   ~ 0
SetNormal
Text Label 2550 2200 0    50   ~ 0
SetAux
Connection ~ 5450 1400
Text Label 7500 1900 0    50   ~ 0
SetGuard
Text Label 7500 2000 0    50   ~ 0
ResetGuard
Text Label 7500 2150 0    50   ~ 0
SetZeroInput
Text Label 7500 2250 0    50   ~ 0
ResetZeroInput
Text Label 7500 2400 0    50   ~ 0
SetZeroSense
Text Label 7500 2500 0    50   ~ 0
ResetZeroSense
Text Label 7500 2650 0    50   ~ 0
SetZeroLow
Text Label 7500 2750 0    50   ~ 0
ResetZeroLow
Text Label 7500 2900 0    50   ~ 0
SetZeroHigh
Text Label 7500 3000 0    50   ~ 0
ResetZeroHigh
Wire Wire Line
	7450 1900 8150 1900
Wire Wire Line
	7450 2000 8150 2000
Wire Wire Line
	7450 2150 8150 2150
Wire Wire Line
	7450 2250 8150 2250
Wire Wire Line
	7450 2400 8150 2400
Wire Wire Line
	7450 2500 8150 2500
Wire Wire Line
	7450 2650 8150 2650
Wire Wire Line
	7450 2750 8150 2750
Wire Wire Line
	7450 2900 8150 2900
Wire Wire Line
	7450 3000 8150 3000
Text Label 6350 3900 0    50   ~ 0
CurrentLowDirect
Text Label 6350 4050 0    50   ~ 0
CurrentHighDirect1
Text Label 6350 4150 0    50   ~ 0
CurrentHighDirect2
Text Label 6350 4300 0    50   ~ 0
Current1
Text Label 6350 4400 0    50   ~ 0
Current2
Text Label 6350 4500 0    50   ~ 0
Current3
Text Label 6350 4600 0    50   ~ 0
Current4
Text Label 6300 4750 0    50   ~ 0
CurrentLowSignalSet
Text Label 6300 4850 0    50   ~ 0
CurrentLowSignalReset
Text Label 6350 5000 0    50   ~ 0
SetCurrent4
Text Label 6350 5100 0    50   ~ 0
ResetCurrent4
Text Label 6350 5350 0    50   ~ 0
ResetCurrent3
Text Label 6350 5600 0    50   ~ 0
ResetCurrent2
Text Label 6350 5850 0    50   ~ 0
ResetCurrent1
Text Label 6350 5250 0    50   ~ 0
SetCurrent3
Text Label 6350 5500 0    50   ~ 0
SetCurrent2
Text Label 6350 5750 0    50   ~ 0
SetCurrent1
Text Label 6350 6000 0    50   ~ 0
ResetFrontZero
Text Label 6350 6100 0    50   ~ 0
SetFrontZero
Wire Wire Line
	6200 4750 7200 4750
Wire Wire Line
	6200 4850 7200 4850
Wire Wire Line
	6200 4600 7200 4600
Wire Wire Line
	6200 4500 7200 4500
Wire Wire Line
	6200 4400 7200 4400
Wire Wire Line
	6200 4300 7200 4300
Wire Wire Line
	6200 5000 7200 5000
Wire Wire Line
	6200 5100 7200 5100
Wire Wire Line
	6200 5250 7200 5250
Wire Wire Line
	6200 3900 7200 3900
Wire Wire Line
	6200 4050 7200 4050
Wire Wire Line
	6200 4150 7200 4150
Wire Wire Line
	6200 5600 7200 5600
Wire Wire Line
	6200 5750 7200 5750
Wire Wire Line
	6200 5850 7200 5850
Wire Wire Line
	6200 6000 7200 6000
Wire Wire Line
	6200 6100 7200 6100
$Comp
L power:GNDD #PWR?
U 1 1 625FC216
P 10150 850
AR Path="/621BA2C5/625FC216" Ref="#PWR?"  Part="1" 
AR Path="/6246F77A/625FC216" Ref="#PWR?"  Part="1" 
AR Path="/625FC216" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 10150 600 50  0001 C CNN
F 1 "GNDD" H 10150 700 50  0000 C CNN
F 2 "" H 10150 850 50  0001 C CNN
F 3 "" H 10150 850 50  0001 C CNN
	1    10150 850 
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 625FC30F
P 9050 850
AR Path="/621BA2C5/625FC30F" Ref="#PWR?"  Part="1" 
AR Path="/6246F77A/625FC30F" Ref="#PWR?"  Part="1" 
AR Path="/625FC30F" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 9050 600 50  0001 C CNN
F 1 "GNDD" H 9050 700 50  0000 C CNN
F 2 "" H 9050 850 50  0001 C CNN
F 3 "" H 9050 850 50  0001 C CNN
	1    9050 850 
	-1   0    0    1   
$EndComp
Wire Wire Line
	9850 1000 10150 1000
Wire Wire Line
	10150 1000 10150 850 
Wire Wire Line
	9350 1000 9050 1000
Wire Wire Line
	9050 1000 9050 850 
Text Label 8800 2000 0    50   ~ 0
SetZeroInput
Text Label 8800 3000 0    50   ~ 0
SetZeroSense
Text Label 8800 2900 0    50   ~ 0
SetZeroLow
Text Label 8800 3100 0    50   ~ 0
SetZeroHigh
Wire Wire Line
	8500 2800 9350 2800
Wire Wire Line
	8500 2900 9350 2900
Wire Wire Line
	8500 3000 9350 3000
Text Label 8800 2100 0    50   ~ 0
SetNormal
Text Label 8800 2800 0    50   ~ 0
SetAux
Wire Wire Line
	8500 2100 9350 2100
Wire Wire Line
	8500 1900 9350 1900
Text Label 10000 1900 0    50   ~ 0
ResetGuard
Text Label 10000 2800 0    50   ~ 0
ResetAux
Text Label 10000 2100 0    50   ~ 0
ResetNormal
Wire Wire Line
	9850 2100 10950 2100
Wire Wire Line
	9850 1900 10950 1900
Wire Wire Line
	9850 2800 10950 2800
Wire Wire Line
	9850 2900 10950 2900
Wire Wire Line
	9850 3000 10950 3000
Text Label 10000 3100 0    50   ~ 0
ResetZeroHigh
Text Label 10000 2900 0    50   ~ 0
ResetZeroLow
Text Label 10000 3000 0    50   ~ 0
ResetZeroSense
Text Label 10000 2000 0    50   ~ 0
ResetZeroInput
$Comp
L conn_2rows-44pins:Conn_2Rows-42Pins J2
U 1 1 625624C4
P 9550 5050
F 0 "J2" H 9600 6367 50  0000 C CNN
F 1 "Conn_2Rows-42Pins" H 9600 6276 50  0000 C CNN
F 2 "Multiplexor-DVM-Cal:Con_Card_Mux_2" H 9550 5050 50  0001 C CNN
F 3 "~" H 9550 5050 50  0001 C CNN
	1    9550 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 3950 10150 3950
Wire Wire Line
	10150 3950 10150 3800
Wire Wire Line
	9350 3950 9050 3950
Wire Wire Line
	9050 3950 9050 3800
Text Label 8800 5650 0    50   ~ 0
SetCurrent1
Text Label 8800 5550 0    50   ~ 0
SetCurrent2
Text Label 8800 5450 0    50   ~ 0
SetCurrent3
Text Label 8800 5350 0    50   ~ 0
SetCurrent4
Text Label 8800 5250 0    50   ~ 0
SetFrontZero
Text Label 8550 5150 0    50   ~ 0
CurrentLowSignalSet
Wire Wire Line
	8500 5150 9350 5150
Wire Wire Line
	8500 5250 9350 5250
Wire Wire Line
	8500 5350 9350 5350
Wire Wire Line
	8500 5450 9350 5450
Wire Wire Line
	8500 5550 9350 5550
Wire Wire Line
	8500 5650 9350 5650
Text Label 8800 4650 0    50   ~ 0
Current2
Text Label 8800 4550 0    50   ~ 0
Current4
Wire Wire Line
	8500 4650 9350 4650
Wire Wire Line
	8500 4550 9350 4550
Wire Wire Line
	8500 4450 9350 4450
Text Label 10000 4550 0    50   ~ 0
Current3
Text Label 10000 4350 0    50   ~ 0
CurrentLowDirect
Text Label 10000 4450 0    50   ~ 0
CurrentHighDirect1
Text Label 10000 4650 0    50   ~ 0
Current1
Text Label 10000 5150 0    50   ~ 0
CurrentLowSignalReset
Wire Wire Line
	9850 5150 10950 5150
Wire Wire Line
	9850 4650 10950 4650
Wire Wire Line
	9850 4550 10950 4550
Wire Wire Line
	9850 4450 10950 4450
Wire Wire Line
	9850 4350 10950 4350
Wire Wire Line
	9850 5250 10950 5250
Wire Wire Line
	9850 5350 10950 5350
Wire Wire Line
	9850 5450 10950 5450
Wire Wire Line
	9850 5550 10950 5550
Wire Wire Line
	9850 5650 10950 5650
Text Label 10000 5250 0    50   ~ 0
ResetFrontZero
Text Label 10000 5350 0    50   ~ 0
ResetCurrent4
Text Label 10000 5450 0    50   ~ 0
ResetCurrent3
Text Label 8600 4450 0    50   ~ 0
CurrentHighDirect2
Text Label 10000 5550 0    50   ~ 0
ResetCurrent2
Text Label 10000 5650 0    50   ~ 0
ResetCurrent1
$Comp
L power:GND #PWR04
U 1 1 62596F1E
P 10150 3800
F 0 "#PWR04" H 10150 3550 50  0001 C CNN
F 1 "GND" H 10155 3627 50  0000 C CNN
F 2 "" H 10150 3800 50  0001 C CNN
F 3 "" H 10150 3800 50  0001 C CNN
	1    10150 3800
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 62596F74
P 9050 3800
F 0 "#PWR02" H 9050 3550 50  0001 C CNN
F 1 "GND" H 9055 3627 50  0000 C CNN
F 2 "" H 9050 3800 50  0001 C CNN
F 3 "" H 9050 3800 50  0001 C CNN
	1    9050 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6200 5350 7200 5350
Wire Wire Line
	6200 5500 7200 5500
Wire Wire Line
	5150 800  5150 2250
Wire Wire Line
	5250 1100 5250 2400
Wire Wire Line
	5350 1400 5350 2550
Wire Wire Line
	4950 1450 4950 1950
Wire Wire Line
	5050 850  5050 2100
Connection ~ 1550 5200
Wire Wire Line
	4500 5500 2800 5500
Wire Wire Line
	4500 4000 3350 4000
Wire Wire Line
	3350 4000 3350 3650
Wire Wire Line
	3350 3650 1550 3650
Connection ~ 1550 3650
Wire Wire Line
	1550 3650 1550 3600
Wire Wire Line
	4500 4150 1550 4150
Connection ~ 1550 4150
Wire Wire Line
	1550 4150 1550 4100
Wire Wire Line
	3350 4300 3350 4700
Wire Wire Line
	3350 4700 1550 4700
Connection ~ 1550 4700
Wire Wire Line
	1550 4700 1550 4650
Wire Wire Line
	3350 4300 4500 4300
Wire Wire Line
	1550 5200 4500 5200
Wire Wire Line
	4500 5350 1950 5350
Wire Wire Line
	1950 5350 1950 5600
Wire Wire Line
	1950 5600 1550 5600
Connection ~ 1550 5600
Wire Wire Line
	1550 5600 1550 5550
Wire Wire Line
	1550 6050 2800 6050
Wire Wire Line
	2800 5500 2800 6050
Connection ~ 1550 6050
Wire Wire Line
	1550 6050 1550 6000
Text Label 8800 1900 0    50   ~ 0
SetGuard
NoConn ~ 9350 1100
NoConn ~ 9350 1200
NoConn ~ 9350 1300
NoConn ~ 9350 1400
NoConn ~ 9350 1500
NoConn ~ 9350 1600
NoConn ~ 9350 1700
NoConn ~ 9350 1800
NoConn ~ 9350 2200
NoConn ~ 9350 2300
NoConn ~ 9350 2400
NoConn ~ 9350 2500
NoConn ~ 9350 2600
NoConn ~ 9350 2700
NoConn ~ 9850 2700
NoConn ~ 9850 2600
NoConn ~ 9850 2500
NoConn ~ 9850 2400
NoConn ~ 9850 2300
NoConn ~ 9850 2200
NoConn ~ 9850 1800
NoConn ~ 9850 1700
NoConn ~ 9850 1600
NoConn ~ 9850 1500
NoConn ~ 9850 1400
NoConn ~ 9850 1300
NoConn ~ 9850 1200
NoConn ~ 9850 1100
NoConn ~ 9350 4050
NoConn ~ 9350 4150
NoConn ~ 9350 4250
NoConn ~ 9350 4750
NoConn ~ 9350 4950
NoConn ~ 9350 5050
NoConn ~ 9350 5750
NoConn ~ 9350 5850
NoConn ~ 9350 5950
NoConn ~ 9350 6050
NoConn ~ 9850 6050
NoConn ~ 9850 5950
NoConn ~ 9850 5850
NoConn ~ 9850 5750
NoConn ~ 9850 5050
NoConn ~ 9850 4950
NoConn ~ 9850 4750
NoConn ~ 9850 4250
NoConn ~ 9850 4150
NoConn ~ 9850 4050
Connection ~ 1550 2050
Connection ~ 1550 2350
$Comp
L Connector_Generic:Conn_01x02 Guard2
U 1 1 62874384
P 1350 6550
F 0 "Guard2" H 1430 6542 50  0000 L CNN
F 1 "Conn_01x02" H 1430 6451 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 6550 50  0001 C CNN
F 3 "~" H 1350 6550 50  0001 C CNN
	1    1350 6550
	-1   0    0    1   
$EndComp
Wire Wire Line
	1550 6550 1550 6450
Wire Wire Line
	4500 6350 1550 6350
Wire Wire Line
	1550 6350 1550 6450
Connection ~ 1550 6450
Wire Wire Line
	6200 6250 7200 6250
Wire Wire Line
	6200 6350 7200 6350
Wire Wire Line
	6200 6450 7200 6450
Text Label 6350 6450 0    50   ~ 0
CurrentIN
Text Label 6350 6250 0    50   ~ 0
SetGuard2
Text Label 6350 6350 0    50   ~ 0
ResetGuard2
Wire Wire Line
	9350 4350 8500 4350
Wire Wire Line
	9350 4850 8500 4850
Wire Wire Line
	9850 4850 10950 4850
Text Label 8800 4350 0    50   ~ 0
CurrentIN
Text Label 8800 4850 0    50   ~ 0
SetGuard2
Text Label 10000 4850 0    50   ~ 0
ResetGuard2
Wire Wire Line
	9850 3100 10950 3100
Wire Wire Line
	9350 3100 8500 3100
Wire Wire Line
	9850 2000 10950 2000
Wire Wire Line
	9350 2000 8500 2000
$EndSCHEMATC
